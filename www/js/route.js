/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

angular
    .module('realLives_mobile')
    .config(config);
        
function config($stateProvider, $urlRouterProvider) {
    $urlRouterProvider.otherwise('/mobile');

    $stateProvider
        .state('mindex', {
            url: '/mobile',
            templateUrl: 'index.html'
        })
  
        .state('mlogin', {
            url: '/mlogin',
            templateUrl: 'views/login.html',
        })
  
        .state('mregister', {
            url: '/mregister',
            templateUrl: 'views/register.html',
        })
        .state('user', {
            url:'/muserpage',
            templateUrl: 'views/userPage.html',
            controller: 'UserCtrl as userCtrl'
        })
        .state('savedGames', {
            url: '/savedGames',
            templateUrl: 'views/savedGames.html',
            controller: 'UserCtrl as userCtrl'
        })
        .state('dashboard', {
            url: '/dashboard',
            templateUrl: 'views/dashboard.html',
            controller: 'DashboardCtrl as dashboardCtrl',
                resolve: {
                    'PlayerData': ['dashboardDataLayer', function (dashboardDataLayer) {
                                    var data = dashboardDataLayer.getPlayerData();
                                        return data;
                                    }],
                    'EventData': ['dashboardDataLayer', function (dashboardDataLayer) {
                                    var eventData = dashboardDataLayer.getEventData();
                                        return eventData;
                                    }]
                }
        });
  
}